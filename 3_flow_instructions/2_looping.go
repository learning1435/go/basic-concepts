package __flow_instructions

import "fmt"

func main() {
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}

	for i := 0; i < 5; i += 2 {
		fmt.Println(i)
	}

	j := 0
	for ; j < 5; j++ {
		fmt.Println(j)
	}

	k := 0
	for k < 5 {
		fmt.Println(k)
		k++
	}

	l := 0
	for {
		if l > 10 {
			fmt.Println("exit")
			break
		}

		fmt.Println(l)
		l++
	}

Loop:
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			fmt.Println(j)
			fmt.Println("exit")

			if i*j > 10 {
				break Loop
			}
		}
	}

	s := []int{1, 2, 3}
	for k, v := range s {
		fmt.Println(k, v)
	}

	statePopulation := map[string]int{
		"California": 123,
		"Texas":      1231,
		"Florida":    1234,
	}

	for k, v := range statePopulation {
		fmt.Println(k, v)
	}

}
