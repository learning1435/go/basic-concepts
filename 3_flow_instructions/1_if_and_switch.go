package __flow_instructions

import "fmt"

func main() {
	if true {
		fmt.Println("The test is true")
	}

	statePopulation := map[string]int{
		"California": 123,
		"Texas":      1231,
		"Florida":    1234,
	}

	if pop, ok := statePopulation["Florida"]; ok {
		fmt.Println("Florida exists", pop)
	}

	number := 10
	guess := 50

	if guess > number {
		fmt.Println("Guess is bigger then number")
	}

	fmt.Println(number <= guess, number >= guess, number != guess)

	if guess < number || guess == number {
		fmt.Println("Is smaller or equal")
	}

	if guess <= number && guess >= number {
		fmt.Println("Is between")
	}

	if returnValue() {
		fmt.Println("Is always true")
	} else {
		fmt.Println("Else block")
	}

	switch 2 {
	case 1, 3, 5:
		fmt.Println("One")
	case 2:
		fmt.Println("Two")
	default:
		fmt.Println("not one or two")
	}

	i := 10
	switch {
	case i <= 10:
		fmt.Println("Is smaller than 10")
		// Go to next one
		fallthrough
	case i <= 20:
		fmt.Println("Is smaller than 20")

	default:
		fmt.Println("Is greater than 20")
	}

	var b interface{} = 1
	switch b.(type) {
	case int:
		fmt.Println("Is int")
		break
		fmt.Println("No executed")
	case float64:
		fmt.Println("Is float64")
	default:
		fmt.Println("Is another type")
	}

}

func returnValue() bool {
	fmt.Println("Call return value")
	return true
}
