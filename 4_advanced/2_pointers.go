package __advanced

import "fmt"

func main() {
	a := 42
	b := a
	fmt.Println(a, b)
	b = 21
	fmt.Println(a, b)

	var c *int = &a
	fmt.Println(a, b, c)
	a = 49123
	fmt.Println(a, b, c, &a, *c)

	d := [3]int{1, 2, 3}
	e := &d[0]
	f := &d[0]
	fmt.Printf("%v %p %p\n", d, e, f)

	var ms *myStruct
	ms = &myStruct{foo: 54}
	fmt.Println(ms)

	var ms2 *myStruct
	ms = new(myStruct)
	fmt.Println(ms2)

	// Both signature is equal
	(*ms).foo = 42
	ms.foo = 42
	fmt.Println((*ms).foo)

	// Slices and maps are reference/pointers
	var2 := map[string]string{"foo": "bar", "baz": "fuz"}
	var3 := var2
	fmt.Println(var2, var3)
	var3["foo"] = "qua"
	fmt.Println(var2, var3)
}

type myStruct struct {
	foo int
}
