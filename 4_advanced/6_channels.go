package __advanced

import (
	"fmt"
	"sync"
)

var wg2 = sync.WaitGroup{}

func channels() {
	ch := make(chan int)
	wg2.Add(2)
	go func() {
		i := <-ch
		fmt.Println(i)
		wg2.Done()
	}()

	go func() {
		// Copying data
		ch <- 42
		wg2.Done()
	}()

	wg2.Wait()

	readAndWriteChannelExample()
	bufferedChannelExample()
}

func readAndWriteChannelExample() {
	ch := make(chan int)

	wg2.Add(2)

	go func(ch <-chan int) {
		i := <-ch
		fmt.Println(i)
		wg2.Done()
	}(ch)

	go func(ch chan<- int) {
		ch <- 42
		wg2.Wait()
	}(ch)

	wg.Wait()
}

func bufferedChannelExample() {
	// Buffered
	ch := make(chan int, 50)

	wg2.Add(2)

	go func(ch <-chan int) {
		for i := range ch {
			fmt.Println(i)
		}
		wg2.Done()
	}(ch)

	go func(ch chan<- int) {
		ch <- 42
		ch <- 41
		ch <- 40
		close(ch)
		wg2.Wait()
	}(ch)

	wg.Wait()
}
