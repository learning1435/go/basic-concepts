package __advanced

import (
	"bytes"
	"fmt"
)

func advancedInterfaces() {
	fmt.Println("Interfaces")
	var w Writer = ConsoleWriter{}
	w.Write([]byte("Writer"))

	myInt := IntCounter(0)
	var inc Incrementer = &myInt
	for i := 0; i < 10; i++ {
		fmt.Println(inc.Increment())
	}

	// Method to check if can converse between interfaces
	//var wc WriterCloser = BufferedWriterCloser{},
	//r, ok := wc.(Writer)
	//if ok {
	//	fmt.Println(r)
	//} else {
	//	fmt.Println("Conversion failed")
	//}
	var emptyInterface interface{} = BufferedWriterCloser{}
	if wc, ok := emptyInterface.(WriterCloser); ok {
		wc.Write([]byte("Custom text"))
		wc.Close()
	}

	var i interface{} = 0
	switch i.(type) {
	case int:
		fmt.Println("Is int")
	case string:
		fmt.Println("Is string")
	default:
		fmt.Println("Unknown")
	}
}

// Writer Convention for interfaces with one method is adding -er
type Writer interface {
	Write([]byte) (int, error)
}

type Closer interface {
	Close() error
}

type WriterCloser interface {
	Writer
	Closer
}

type ConsoleWriter struct {
}

func (cw ConsoleWriter) Write(data []byte) (int, error) {
	n, err := fmt.Println(string(data))
	return n, err
}

func (bwc *BufferedWriterCloser) Write(data []byte) (int, error) {
	n, err := bwc.buffer.Write(data)
	if err != nil {
		return 0, err
	}

	v := make([]byte, 8)
	for bwc.buffer.Len() > 8 {
		_, err := bwc.buffer.Read(v)
		if err != nil {
			return 0, nil
		}
		_, err = fmt.Println(string(v))
		if err != nil {
			return 0, nil
		}
	}

	return n, nil
}

type BufferedWriterCloser struct {
	buffer *bytes.Buffer
}

type Incrementer interface {
	Increment() int
}

type IntCounter int

func (ic *IntCounter) Increment() int {
	*ic++
	return int(*ic)
}

// Best practices
// Use many, small interfaces
// Don't export interfaces for types that will be consumed
// Do export interfaces for types that will be used by package
