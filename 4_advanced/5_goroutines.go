package __advanced

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func goroutines() {
	go sayHello()
	time.Sleep(100 * time.Millisecond)

	// Referenced - print Goodbye
	var msg = "Hello"
	go func() {
		fmt.Println(msg)
	}()
	msg = "Goodbye"
	time.Sleep(100 * time.Millisecond)

	// Copied - print Hello
	var msg2 = "Hello"
	go func(msg string) {
		fmt.Println(msg)
	}(msg2)
	msg2 = "Goodbye"
	time.Sleep(100 * time.Millisecond)

	// Synchronising
	var wg = sync.WaitGroup{}

	wg.Add(1)
	go func() {
		fmt.Println("Test 1")
		wg.Done()
	}()

	multipleWaitGroup()

	wg.Wait()
}

func sayHello() {
	fmt.Println("Hello")
}

var wg = sync.WaitGroup{}
var counter = 0
var m = sync.RWMutex{}

func multipleWaitGroup() {
	runtime.GOMAXPROCS(100)
	for i := 0; i < 10; i++ {
		wg.Add(2)
		m.RLock()
		go sayHelloAsync()
		m.Lock()
		go incrementAsync()
	}
	wg.Wait()
}

func sayHelloAsync() {
	fmt.Println("Hello")
	m.RUnlock()
	wg.Done()
}

func incrementAsync() {
	counter++
	m.Unlock()
	wg.Done()
}
