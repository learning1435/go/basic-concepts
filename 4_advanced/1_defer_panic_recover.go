package __advanced

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	// Defer
	fmt.Println("start")
	defer fmt.Println("middle")
	fmt.Println("end")

	res, err := http.Get("http://googe.com/robots.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()
	robots, err := ioutil.ReadAll(res.Body)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("%s", robots)

	a := "start"
	// It will print start, cause defer is called with
	// that its defined while calling defer
	defer fmt.Println(a)
	a = "end"

	// Panic
	//a, b := 1, 0
	//ans := a /b
	//fmt.Println(ans)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Response) {
		w.Write([]byte("Hello Go!"))
	})

	errServer := http.ListenAndServe(":8080", nil)
	if errServer != nil {
		panic(errServer.Error())
	}

	// Won't work - panicer stop main
	//fmt.Println("start")
	//defer func() {
	//	if err := recover(); err != nil {
	//		log.Println("Error", err)
	//	}
	//}()
	//panic("something happened")
	//fmt.Println("end")

	// End will be printed
	fmt.Println("start")
	panicer()
	fmt.Println("end")
}

func panicer() {
	fmt.Println("start")
	defer func() {
		if err := recover(); err != nil {
			log.Println("Error", err)
		}
	}()
	panic("something happened")
	fmt.Println("end")
}
