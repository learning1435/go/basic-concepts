package __advanced

import "fmt"

func main() {
	sayMassage("Hi!")

	sum(1, 2, 3, 4, 5)
	sum1 := sumRef(1, 2, 3, 4, 5)
	fmt.Println(sum1)

	sum2 := sumWithNamedReturn(1, 2, 3, 4, 5)
	fmt.Println(sum2)

	divide1, err := divide(1.0, 0.0)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(divide1)

	divide2, _ := divide(1.0, 5.0)
	fmt.Println(divide2)

	func() {
		fmt.Println("Anonymous function")
	}()

	function := func() {
		fmt.Println("Test")
	}

	function()

	// Methods
	greeterInst := greeter{
		greeting: "Test",
		name:     "test",
	}

	greeterInst.greet()
	greeterInst.changeName("new name")
}

func sayMassage(msg string) {
	fmt.Println(msg)
}

func sum(values ...int) {
	fmt.Println(values)
	result := 0
	for _, v := range values {
		result += v
	}

	fmt.Println("the sum is", result)
}

// In Go if we return pointer to address, then variable is
// promoted in shared memory (it is not free when execution is out of code block)
// so we don't need to be afraid we return to pointer to address when other variable is stored
func sumRef(values ...int) *int {
	fmt.Println(values)
	result := 0
	for _, v := range values {
		result += v
	}

	fmt.Println("the sum is", result)
	return &result
}

func sumWithNamedReturn(values ...int) (result int) {
	fmt.Println(values)
	for _, v := range values {
		result += v
	}

	fmt.Println("the sum is", result)
	return
}

func divide(a, b float64) (float64, error) {
	if b <= 0.0 {
		return 0.0, fmt.Errorf("cannot devide by zero")
	}

	return a / b, nil
}

type greeter struct {
	greeting string
	name     string
}

// g is not reference, its copy
func (g greeter) greet() {
	fmt.Println(g.name, g.greeting)
}

func (g *greeter) changeName(newName string) {
	g.name = newName
}
