# Run app
`go run main.go`

# Build app
`go build main.go`

# Install app
`go install main.go`