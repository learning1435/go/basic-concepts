package __variables

import "fmt"

func variablesPrimitives() {
	// Boolean types
	var n bool = true
	fmt.Printf("%v, %T\n", n, n)

	n = 1 == 1
	n = 1 == 2

	// Empty value = false
	var emptyValue bool
	fmt.Printf("%v, %T\n", emptyValue, emptyValue)

	// Int types
	var int8Byte int8 = -8
	fmt.Printf("%v, %T\n", int8Byte, int8Byte)
	var int16Byte int16 = -16
	fmt.Printf("%v, %T\n", int16Byte, int16Byte)
	var int32Byte int32 = -32
	fmt.Printf("%v, %T\n", int32Byte, int32Byte)
	var int64Byte int64 = 64
	fmt.Printf("%v, %T\n", int64Byte, int64Byte)

	var uint8Byte uint8 = 8
	fmt.Printf("%v, %T\n", uint8Byte, uint8Byte)
	var uint16Byte uint16 = 16
	fmt.Printf("%v, %T\n", uint16Byte, uint16Byte)
	var uint32Byte uint32 = 32
	fmt.Printf("%v, %T\n", uint32Byte, uint32Byte)
	var uint64Byte uint64 = 64
	fmt.Printf("%v, %T\n", uint64Byte, uint64Byte)

	// Basic maths operators
	a := 10
	b := 3

	fmt.Println(a + b)
	fmt.Println(a - b)
	fmt.Println(a * b)
	fmt.Println(a / b) // return integer cause integer types both variables
	fmt.Println(a % b) // modulo

	var c int = 10100
	var d int8 = 1
	// fmt.Println(c + d) <- ERROR not compatible types
	fmt.Println(c + int(d))

	// Binary operators
	var e int = 2
	var f int = 3
	fmt.Println(e & f)  // 2 => 10 // BOTH
	fmt.Println(e | f)  // 3 => 11 // AT LEAST ONE
	fmt.Println(e ^ f)  // 1 => 01 // NOT ONE OF THEM
	fmt.Println(e &^ f) // 0 => 00 // NOT BOTH

	g := 8              // 1000
	fmt.Println(g << 4) // 1000
	fmt.Println(g >> 3) // 0001

	// Float type
	var h float64 = 3.14
	h = 13.7e72
	h = 2.1e14
	fmt.Printf("%v, %T\n", h, h)

	// Complex type
	var i complex64 = 1 + 2i
	fmt.Printf("%v, %T\n", i, i)
	fmt.Printf("%v, %T\n", real(i), real(i))
	fmt.Printf("%v, %T\n", imag(i), imag(i))

	var j complex128 = complex(5, 12)
	fmt.Printf("%v, %T\n", j, j)

	// String types
	k := "this is a string"
	fmt.Printf("%v, %T\n", k, k)
	fmt.Printf("%v, %T\n", k[0], k[0])

	k += " and other string"
	l := []byte(k)
	fmt.Printf("%v, %T\n", l, l)

	// Rune type - alias for int32
	m := 'a'
	fmt.Printf("%v, %T\n", m, m)
}
