package __variables

import (
	"fmt"
	"strconv"
)

// Starting variable with upper case means it will be exported
// Starting variable with lower case means it will be available in scope

func variablesDeclaringVariables() {
	var a int = 42
	a = 42

	var b int = 4000

	c := 999

	var d float32 = 123.01
	var e float64 = 123123123.123213

	var f string = "some text"

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)

	// Print variable and its type
	fmt.Printf("%v, %T", a, a)

	var (
		test1 string = "test1"
		test2 string = "test1"
		test3        = "test1"
		test4        = "test1"
	)

	fmt.Println(test1)
	fmt.Println(test2)
	fmt.Println(test3)
	fmt.Println(test4)

	// String conversion
	var integer int = 42
	fmt.Printf("%v, %T", integer, integer)

	// String conversion
	var stringInteger string
	stringInteger = strconv.Itoa(integer)

	fmt.Printf("%v, %T", stringInteger, stringInteger)
}

// Best practices
// Don't create goroutines in libraries - let consumer control concurrency
// When creating a goroutine, know how it will end - avoid subtle memory leaks
// Check for race conditions at compile time
