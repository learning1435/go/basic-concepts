package __variables

import "fmt"

const (
	_  = iota
	KB = 1 << (10 * iota)
	MB
	GB
	TB
)

func variablesConstants() {
	// Cannot be re-assigned
	// Cannot be dynamic assigned
	const var1 int = 1
	const var2 bool = true
	const var3 string = "string"
	const var4 float32 = 1312312.312
	fmt.Printf("%v, %T", var1, var1)
	fmt.Printf("%v, %T", var2, var2)
	fmt.Printf("%v, %T", var3, var3)
	fmt.Printf("%v, %T", var4, var4)

	// iota is local scoped - its value in incrementing only in scope
	// and its reset when it is out of scope
	const var5 = iota
	const var6 = iota
	fmt.Printf("%v, %T", var5, var5)
	fmt.Printf("%v, %T", var6, var6)

	const (
		var7 = 7
		var8 = 8
		var9 = 9
	)

	fmt.Printf("%v, %T", var7, var7)
	fmt.Printf("%v, %T", var8, var8)
	fmt.Printf("%v, %T", var9, var9)

	fileSize := 40000000000
	fmt.Printf("File size is %.2f GB", fileSize/GB)

}
