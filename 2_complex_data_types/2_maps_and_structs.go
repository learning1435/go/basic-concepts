package __complex_data_types

import (
	"fmt"
	"reflect"
)

type Doctor struct {
	number     int
	actorName  string
	companions []string
}

type Animal struct {
	Name   string `required max: "100"`
	Origin string
}

type Bird struct {
	Animal
	Speed  float32
	CanFly bool
}

func complexDataTypesMapsAndStructs() {
	// Maps
	statePopulation := map[string]int{
		"California": 123,
		"Texas":      1231,
		"Florida":    1234,
	}

	fmt.Println(statePopulation)
	m := map[[3]int]string{}
	fmt.Println(m)

	var1 := make(map[string]int)
	var1 = map[string]int{
		"test": 1,
	}
	fmt.Println(var1)

	fmt.Println(statePopulation["California"])
	statePopulation["Washingtion"] = 124512

	delete(statePopulation, "Washingtion")
	fmt.Println(statePopulation)

	pop, ok := statePopulation["Oho"]
	fmt.Println(pop, ok)

	_, exists := statePopulation["Oho"]
	fmt.Println(exists)

	fmt.Println(len(statePopulation))

	// Its reference type
	statePopulationCopy := statePopulation
	delete(statePopulation, "Florida")
	fmt.Println(len(statePopulationCopy))

	// Structs

	aDoctor := Doctor{
		actorName: "test",
		number:    123,
		companions: []string{
			"te1",
			"te2",
		},
	}

	fmt.Println(aDoctor)
	fmt.Println(aDoctor.actorName)

	customStruct := struct{ name string }{name: "Test"}
	fmt.Println(customStruct)

	// Create copy
	customStruct2 := customStruct
	fmt.Println(customStruct.name)
	fmt.Println(customStruct2.name)

	bird := Bird{}
	bird.Name = "BIrd"
	bird.Origin = "kanu"
	bird.Speed = 32.12
	bird.CanFly = true
	fmt.Println(bird)

	animal := Animal{Name: "test"}
	t := reflect.TypeOf(Animal{})
	field, _ := t.FieldByName("Name")
	fmt.Println(field.Tag)
	fmt.Println(animal)
}
