package __complex_data_types

import "fmt"

func complexDataTypesArraysAndSlices() {

	// Arrays - no reference type
	var1 := [3]int{1, 2, 3}
	fmt.Printf("Grades: %v", var1)

	var2 := [...]int{1, 2, 3}
	fmt.Printf("Grades: %v", var2)

	var var3 [3]string
	var3[0] = "test1"
	var3[1] = "test2"
	var3[2] = "test3"
	fmt.Printf("Grades: %v", var3)

	fmt.Printf("Len: %v", len(var3))

	var var4 [3][3]int = [3][3]int{[3]int{1, 0, 0}, [3]int{1, 0, 0}, [3]int{1, 0, 0}}
	fmt.Printf("Len: %v", var4)

	var var5 = [...]int{1, 2, 3}
	var var6 = var5
	var6[1] = 123

	fmt.Printf("Var5: %v", var5)
	fmt.Printf("Var6: %v", var6)

	var var7 = [...]int{1, 2, 3}
	var var8 = &var7
	var8[1] = 123

	fmt.Printf("Var5: %v", var7)
	fmt.Printf("Var6: %v", var8)

	// Slices - reference type
	var9 := []int{1, 2, 3}
	fmt.Printf("var9: %v", var9)
	fmt.Printf("Len: %v", len(var9))
	fmt.Printf("Cap: %v", cap(var9))

	var var10 = []int{1, 2, 3}
	var var11 = var10
	// NO REFERENCE
	var var12 = var10[1:]
	var11[1] = 123
	fmt.Printf("var10: %v", var10)
	fmt.Printf("var11: %v", var11)
	fmt.Printf("var12: %v", var12)

	var13 := make([]int, 3, 100)
	fmt.Printf("var13: %v", var13)
	fmt.Printf("var13 Len: %v", len(var13))
	fmt.Printf("var13 Cap: %v", cap(var13))
	var13 = append(var13, 20)
	// Spread operator
	var13 = append(var13, []int{1, 2, 3, 4, 5}...)
	var14 := var13[1:]                       // Remove last value
	var15 := var13[:len(var13)-1]            // Remove last value
	var16 := append(var13[:2], var13[3:]...) // remove middle value
	fmt.Printf("var14: %v", var14)
	fmt.Printf("var15: %v", var15)
	fmt.Printf("var16: %v", var16)
}
